package com.medetzhakupov.todo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.medetzhakupov.todo.provider.Images;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new MyAdater());
    }



    class MyAdater extends RecyclerView.Adapter<MyAdater.VH> {


        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.recyler_view_item, parent, false);
//            ImageView imageView = new ImageView(parent.getContext());
//            imageView.setMaxHeight(100);
//            imageView.setLayoutParams(
//                    new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                            ViewGroup.LayoutParams.WRAP_CONTENT));
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(VH holder, int position) {
            Glide.with(holder.itemView.getContext())
                    .load(Images.imageThumbUrls[position])
                    .centerCrop()
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.empty_photo)
                    .into((ImageView) holder.itemView.findViewById(R.id.imageView));
//                    .into((ImageView) holder.itemView);
        }

        @Override
        public int getItemCount() {
            return Images.imageThumbUrls.length;
        }

        class VH extends RecyclerView.ViewHolder {
            public VH(View view){
                super(view);
            }
        }
    }
}
